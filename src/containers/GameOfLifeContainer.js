import React from 'react';
import Board from '../components/Board';
import {connect} from "react-redux";
import {setAliveCellsCount, setBoardSize, setNextStep, toggleCell} from "../redux/actions/board";
import PropTypes from 'prop-types';
import SizePicker from "../components/SizePicker";
import {getRandomInt} from "../utils/math";
import AliveCellsGenerator from "../components/AliveCellsGenerator";
import NextStep from "../components/NextStep";

/**
 * Root component of the app
 */
class GameOfLifeContainer extends React.Component
{
    static propTypes = {
        board: PropTypes.array.isRequired,
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        toggleCell: PropTypes.func.isRequired,
        setBoardSize: PropTypes.func.isRequired,
        setAliveCellsCount: PropTypes.func.isRequired,
        setNextStep: PropTypes.func.isRequired,
    };

    /**
     * Generates random number of alive cells and renders them
     * @param event
     */
    generateRandomAliveCells(event)
    {
        // Prevent form submit
        event.preventDefault();

        // Get random number of alive cells
        const randomAliveCellsCount = getRandomInt(0, this.props.width * this.props.height);

        // Save alive cells to store
        this.props.setAliveCellsCount(randomAliveCellsCount);
    }

    /**
     * Handles submit of next step form
     * @param event
     */
    onNextStepSubmit(event)
    {
        // Prevent form submit
        event.preventDefault();

        // Render next step
        this.props.setNextStep();
    }

    render()
    {
        return (
            <div id="game-of-life-container">
                <Board
                    board={ this.props.board }
                    toggleCell={ this.props.toggleCell }
                />

                <SizePicker
                    width={ this.props.width }
                    height={ this.props.height }
                    onSubmit={ this.props.setBoardSize }
                />

                <AliveCellsGenerator onSubmit={ this.generateRandomAliveCells.bind(this) }/>

                <NextStep onSubmit={ this.onNextStepSubmit.bind(this) } />
            </div>
        );
    }
}

export default connect((state) => ({
    board: state.board.board,
    width: state.board.width,
    height: state.board.height,
}), (dispatch) => ({
    toggleCell: (cellIndexX, cellIndexY) => dispatch(toggleCell(cellIndexX, cellIndexY)),
    setBoardSize: (width, height) => dispatch(setBoardSize(width, height)),
    setAliveCellsCount: (count) => dispatch(setAliveCellsCount(count)),
    setNextStep: () => dispatch(setNextStep()),
}))(GameOfLifeContainer);