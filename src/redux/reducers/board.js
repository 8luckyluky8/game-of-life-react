import {
    SET_ALIVE_CELLS_COUNT_ACTION_TYPE,
    SET_BOARD_SIZE_ACTION_TYPE, SET_NEXT_STEP_ACTION_TYPE,
    TOGGLE_CELL_ACTION_TYPE
} from "../actions/board";
import {getRandomInt} from "../../utils/math";
import {getCellState} from "../../utils/rules";

const DEFAULT_BOARD_HEIGHT = 10;
const DEFAULT_BOARD_WIDTH = 10;

/**
 * Returns random coords of alive cells on board (format "x:y")
 * @param count
 * @param width
 * @param height
 * @returns {Array}
 */
const getRandomBoardCoords = (count, width, height) => {
    const coords = [];
    let x, y;

    for (let i = 0; i < count; i++)
    {
        x = getRandomInt(0, width - 1);
        y = getRandomInt(0, height - 1);

        coords.push(`${x}:${y}`);
    }

    return coords;
};

/**
 * Init board with provided number of alive cells
 * @param width
 * @param height
 * @param aliveCells
 * @returns {Array}
 */
export const initBoard = (width, height, aliveCells = 0) => {
    const board = [];
    let aliveCellsCoords = [];

    const boardSize = width * height;
    if (aliveCells > boardSize)
    {
        aliveCells = boardSize;
    }
    if (aliveCells > 0)
    {
        aliveCellsCoords = getRandomBoardCoords(aliveCells, width, height);
    }

    for (let i = 0; i < height; i++)
    {
        board[i] = [];

        for (let j = 0; j < width; j++)
        {
            board[i][j] = false;
            if (aliveCells > 0 && aliveCellsCoords.indexOf(`${j}:${i}`) !== -1)
            {
                board[i][j] = true;
            }
        }
    }

    return board;
};

/**
 * Returns number of alive cells on board
 * @param board
 */
export const getAliveCellsCount = (board) => {
    return board.filter((row) => {
        return row.filter((cell) => cell).length > 0;
    }).length;
};

/**
 * Copy content of old board to new board
 * @param newBoard
 * @param oldBoard
 * @returns {Array|Object|*}
 */
const copyBoardData = (newBoard, oldBoard) => {
    return newBoard.map((row, y) => {
        if (y > (oldBoard.length - 1))
        {
            return row;
        }

        return row.map((cell, x) => {
            if (x >= (oldBoard[y].length))
            {
                return cell;
            }

            return oldBoard[y][x];
        });
    });
};

// Create initial board and set all cells to false (dead)
const defaultBoard = initBoard(DEFAULT_BOARD_HEIGHT, DEFAULT_BOARD_WIDTH);

export const defaultState = {
    board: defaultBoard,
    width: DEFAULT_BOARD_WIDTH,
    height: DEFAULT_BOARD_HEIGHT,
};

const board = (state = defaultState, action) => {
    switch (action.type)
    {
        case TOGGLE_CELL_ACTION_TYPE:
            const board = state.board.map((row, y) => {
                return row.map((cell, x) => {
                    if (x === action.cellIndexX && y === action.cellIndexY)
                    {
                        return !cell;
                    }

                    return cell;
                });
            });

            return { ...state, board };

        case SET_BOARD_SIZE_ACTION_TYPE:
            const { width, height } = action;

            let newBoard = state.board;
            if (width !== state.width || height !== state.height)
            {
                newBoard = initBoard(width, height);
                newBoard = copyBoardData(newBoard, state.board);
            }

            return { ...state, board: newBoard, width, height };

        case SET_ALIVE_CELLS_COUNT_ACTION_TYPE:
            const boardWithAliveCells = initBoard(state.width, state.height, action.aliveCellsCount);

            return { ...state, board: boardWithAliveCells };

        case SET_NEXT_STEP_ACTION_TYPE:
            const nextStepBoard = state.board.map((row, y) => {
                return row.map((cell, x) => {
                    return getCellState(x, y, state.board);
                });
            });

            return { ...state, board: nextStepBoard };

        default:
            return state;
    }
};

export default board;