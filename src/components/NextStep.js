import React from 'react';
import PropTypes from 'prop-types';

const NextStep = ({ onSubmit }) => (
    <form onSubmit={ onSubmit }>
        <input type="submit" value="Next step" />
    </form>
);

NextStep.propTypes = {
    onSubmit: PropTypes.func.isRequired,
};

export default NextStep;