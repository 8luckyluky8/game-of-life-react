import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import GameOfLifeContainer from './containers/GameOfLifeContainer';
import {createStore} from "redux";
import reducer from './redux/reducers';

const store = createStore(
    reducer,
    // Redux Dev Tools integration
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <Provider store={ store }>
        <GameOfLifeContainer />
    </Provider>,
    document.getElementById('root')
);
