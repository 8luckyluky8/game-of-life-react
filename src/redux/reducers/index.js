import { combineReducers } from 'redux';
import board from './board';

const app = combineReducers({
    board,
});

export default app;