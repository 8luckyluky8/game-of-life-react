import { getCellAliveNeighboursCount, getCellState } from '../utils/rules';
import boardReducer, { defaultState, initBoard, getAliveCellsCount } from '../redux/reducers/board';
import { setNextStep } from '../redux/actions/board';

describe('One cell on empty board', () => {
    const board = initBoard(10, 10, 0);
    const x = 0;
    const y = 0;
    board[y][x] = true;

    it('should have no alive neighbors', () => {
        expect(getCellAliveNeighboursCount(x, y, board)).toEqual(0);
    });

    it('should die', () => {
        expect(getCellState(x, y, board)).toEqual(false);

        defaultState.board = board;
        const nextState = boardReducer(defaultState, setNextStep());
        expect(getAliveCellsCount(nextState.board)).toEqual(0);
    });
});

describe('Three cells next to each other', () => {
    const board = initBoard(10, 10, 0);
    board[0][0] = true;
    board[0][1] = true;
    board[0][2] = true;

    it('should have according alive neighbours', () => {
        expect(getCellAliveNeighboursCount(0, 0, board)).toEqual(1);
        expect(getCellAliveNeighboursCount(1, 0, board)).toEqual(2);
        expect(getCellAliveNeighboursCount(2, 0, board)).toEqual(1);
    });

    it('two should die', () => {
        expect(getCellState(0, 0, board)).toEqual(false);
        expect(getCellState(1, 0, board)).toEqual(true);
        expect(getCellState(2, 0, board)).toEqual(false);
    });

    it('should result in two alive cells', () => {
        defaultState.board = board;
        const nextState = boardReducer(defaultState, setNextStep());
        expect(getAliveCellsCount(nextState.board)).toEqual(2);

        expect(getCellState(0, 0, board)).toEqual(false);
        expect(getCellState(1, 0, board)).toEqual(true);
        expect(getCellState(2, 0, board)).toEqual(false);
        expect(getCellState(1, 1, board)).toEqual(true);
    });
});

describe('Three cells not next to each other', () => {
    const board = initBoard(10, 10, 0);
    board[0][0] = true;
    board[0][2] = true;
    board[0][4] = true;

    it('should have no alive neighbours', () => {
        expect(getCellAliveNeighboursCount(0, 0, board)).toEqual(0);
        expect(getCellAliveNeighboursCount(2, 0, board)).toEqual(0);
        expect(getCellAliveNeighboursCount(4, 0, board)).toEqual(0);
    });

    it('should all die', () => {
        expect(getCellState(0, 0, board)).toEqual(false);
        expect(getCellState(2, 0, board)).toEqual(false);
        expect(getCellState(4, 0, board)).toEqual(false);
    });

    it('should result in no alive cells', () => {
        defaultState.board = board;
        const nextState = boardReducer(defaultState, setNextStep());
        expect(getAliveCellsCount(nextState.board)).toEqual(0);
    });
});

describe('Two cells next to each other', () => {
    const board = initBoard(10, 10, 0);
    board[0][0] = true;
    board[0][1] = true;

    it('should have according alive neighbours', () => {
        expect(getCellAliveNeighboursCount(0, 0, board)).toEqual(1);
        expect(getCellAliveNeighboursCount(1, 0, board)).toEqual(1);
    });

    it('should all die', () => {
        expect(getCellState(0, 0, board)).toEqual(false);
        expect(getCellState(1, 0, board)).toEqual(false);
    });

    it('should result in no alive cells', () => {
        defaultState.board = board;
        const nextState = boardReducer(defaultState, setNextStep());
        expect(getAliveCellsCount(nextState.board)).toEqual(0);
    });
});