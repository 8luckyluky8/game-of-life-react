export const TOGGLE_CELL_ACTION_TYPE = 'TOGGLE_CELL';
export const toggleCell = (cellIndexX, cellIndexY) => ({
    type: TOGGLE_CELL_ACTION_TYPE,
    cellIndexX,
    cellIndexY,
});

export const SET_BOARD_SIZE_ACTION_TYPE = 'SET_BOARD_SIZE';
export const setBoardSize = (width, height) => ({
    type: SET_BOARD_SIZE_ACTION_TYPE,
    width,
    height,
});

export const SET_ALIVE_CELLS_COUNT_ACTION_TYPE = 'SET_ALIVES_CELLS_COUNT';
export const setAliveCellsCount = (aliveCellsCount) => ({
    type: SET_ALIVE_CELLS_COUNT_ACTION_TYPE,
    aliveCellsCount,
});

export const SET_NEXT_STEP_ACTION_TYPE = 'SET_NEXT_STEP';
export const setNextStep = () => ({
    type: SET_NEXT_STEP_ACTION_TYPE,
});