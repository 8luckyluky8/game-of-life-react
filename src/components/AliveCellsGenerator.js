import React from 'react';
import PropTypes from 'prop-types';

const AliveCellsGenerator = ({ onSubmit }) => (
    <form onSubmit={ onSubmit }>
        <input type="submit" value="Generate alive cells" />
    </form>
);

AliveCellsGenerator.propTypes = {
    onSubmit: PropTypes.func.isRequired,
};

export default AliveCellsGenerator;