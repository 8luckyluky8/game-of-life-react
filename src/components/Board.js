import React from 'react';
import PropTypes from 'prop-types';

const Board = ({ board, toggleCell }) => (
    <div>
        <table>
            <tbody>
            { board.map((row, y) => (
                <tr key={ y }>
                    { row.map((cell, x) => (
                        <td key={ x } className={ ['cell ', cell ? 'aliveCell' : 'deadCell'].join(' ') } onClick={ () => toggleCell(x, y) } />
                    )) }
                </tr>
            )) }
            </tbody>
        </table>
    </div>
);

Board.propTypes = {
    board: PropTypes.array.isRequired,
    toggleCell: PropTypes.func.isRequired,
};

export default Board;