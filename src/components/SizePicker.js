import React from 'react';
import PropTypes from 'prop-types';

const MAX_SIZE = 100;
const MAX_SIZE_LENGTH = 3;

export default class SizePicker extends React.Component
{
    static propTypes = {
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        onSubmit: PropTypes.func.isRequired,
    };

    constructor(props)
    {
        super(props);

        const { width, height } = props;

        this.state = {
            width,
            height,
        };
    }

    /**
     * Is the new size of board valid according to min and max size?
     * @param size
     * @returns {boolean}
     */
    isValidSize(size)
    {
        if (`${size}`.length > MAX_SIZE_LENGTH)
        {
            return false;
        }
        if (size < 0)
        {
            return false;
        }
        if (size > MAX_SIZE)
        {
            return false;
        }

        return true;
    }

    onWidthChange(event)
    {
        const width = parseInt(event.target.value);
        if (!this.isValidSize(width))
        {
            return;
        }

        this.setState({
            width,
        });
    }

    onHeightChange(event)
    {
        const height = parseInt(event.target.value);
        if (!this.isValidSize(height))
        {
            return;
        }

        this.setState({
            height,
        });
    }

    onSubmit(event)
    {
        event.preventDefault();
        this.props.onSubmit(this.state.width, this.state.height);
    }

    render()
    {
        return (
            <form onSubmit={ this.onSubmit.bind(this) }>
                <label htmlFor="board-width">Width</label>
                <input id="board-width" name="board-width" type="number" value={ this.state.width } onChange={ this.onWidthChange.bind(this) } />

                <label htmlFor="board-height">Height</label>
                <input id="board-height" name="board-height" type="number" value={ this.state.height } onChange={ this.onHeightChange.bind(this) } />

                <input type="submit" value="Save" />
            </form>
        );
    }
}