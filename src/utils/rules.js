/**
 * Return number of alive neighbours of cell on given coords.
 * @param x
 * @param y
 * @param board
 * @returns {number}
 */
export const getCellAliveNeighboursCount = (x, y, board) => {
    let cellNeighboursCount = 0;
    const boardHeight = board.length;
    const boardWidth = board.length ? board[0].length : 0;

    for (let i = y - 1; i <= y + 1; i++)
    {
        if (i < 0 || i >= boardHeight)
        {
            continue;
        }

        for (let j = x - 1; j <= x + 1; j++)
        {
            if (i === y && j === x)
            {
                continue;
            }
            if (j < 0 || j >= boardWidth)
            {
                continue;
            }

            if (board[i][j])
            {
                cellNeighboursCount++;
            }
        }
    }

    return cellNeighboursCount;
};

/**
 * Returns new cell state according to game rules
 * (dead cell needs 3 alive neighbours to become alive, alive cell needs 2 or 3 alive neighbours to stay alive; other than that, cell dies)
 * @param x
 * @param y
 * @param board
 * @returns {boolean}
 */
export const getCellState = (x, y, board) => {
    let cellNeighboursCount = getCellAliveNeighboursCount(x, y, board);

    if (cellNeighboursCount === 3)
    {
        return true;
    }
    if (cellNeighboursCount === 2)
    {
        return board[y][x] || false;
    }

    return false;
};