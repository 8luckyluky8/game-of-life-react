import boardReducer, { defaultState, initBoard, getAliveCellsCount } from '../redux/reducers/board';
import { setNextStep, toggleCell, setBoardSize } from '../redux/actions/board';

describe('Empty board', () => {
    const board = initBoard(10, 10, 0);

    it('should have no alive cells', () => {
        expect(getAliveCellsCount(board)).toEqual(0);
    });

    it('should result in no alive cells', () => {
        defaultState.board = board;
        const nextState = boardReducer(defaultState, setNextStep());
        expect(getAliveCellsCount(nextState.board)).toEqual(0);
    });
});

describe('Toggle cell', () => {
    const board = initBoard(10, 10, 0);

    it('which is alive should result in dead cell', () => {
        board[0][0] = true;
        defaultState.board = board;
        expect(getAliveCellsCount(defaultState.board)).toEqual(1);
        const nextState = boardReducer(defaultState, toggleCell(0, 0));
        expect(getAliveCellsCount(nextState.board)).toEqual(0);
        expect(nextState.board[0][0]).toEqual(false);
    });

    it('which is dead should result in alive cell', () => {
        board[0][0] = false;
        defaultState.board = board;
        expect(getAliveCellsCount(defaultState.board)).toEqual(0);
        const nextState = boardReducer(defaultState, toggleCell(0, 0));
        expect(getAliveCellsCount(nextState.board)).toEqual(1);
        expect(nextState.board[0][0]).toEqual(true);
    });

    it('out of bounds should not crash', () => {
        board[0][0] = false;
        defaultState.board = board;
        expect(getAliveCellsCount(defaultState.board)).toEqual(0);
        const nextState = boardReducer(defaultState, toggleCell(20, 20));
        expect(getAliveCellsCount(nextState.board)).toEqual(0);
    });
});

describe('Set board size', () => {
    const width = 10;
    const newWidth = 40;
    const height = 15;
    const newHeight = 30;

    it('should change board size', () => {
        defaultState.board = initBoard(width, height);
        expect(defaultState.board.length).toEqual(height);
        expect(defaultState.board[0].length).toEqual(width);
        const nextState = boardReducer(defaultState, setBoardSize(newWidth, newHeight));
        expect(nextState.board.length).toEqual(newHeight);
        expect(nextState.board[0].length).toEqual(newWidth);
    });

    it('should preserve cells state', () => {
        defaultState.board = initBoard(width, height);
        defaultState.board[0][0] = true;
        defaultState.board[height - 1][width - 1] = true;
        const nextState = boardReducer(defaultState, setBoardSize(newWidth, newHeight));
        expect(nextState.board[0][0]).toEqual(true);
        expect(nextState.board[height - 1][width - 1]).toEqual(true);
    });
});